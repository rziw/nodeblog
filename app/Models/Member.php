<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'name', 'address', 'map_url', 'status', 'phone', 'image', 'email', 'fax', 'score', 'city_id', 'category_id'
    ];

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function collections()
    {
        return $this->hasMany('App\Models\Collection');
    }
}
