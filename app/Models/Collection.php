<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $fillable = [
        'name', 'description', 'image', 'status', 'tag', 'priority', 'member_id'
    ];

    public function member()
    {
        return $this->belongsTo('App\Models\Member');
    }

    public function sets()
    {
        return $this->HasMany('App\Models\Set');
    }
}
