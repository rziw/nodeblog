<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $fillable = [
        'name', 'description', 'image', 'status', 'tag', 'priority', 'set_id'
    ];

    public function set()
    {
        return $this->belongsTo('App\Models\Set');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Item');
    }
}
