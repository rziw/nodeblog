<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccess extends Model
{
    protected $fillable = [
        'access_id', 'user_id', 'member_id'
    ];

    public function access()
    {
        return $this->belongsTo('App\Models\Access');
    }
}
