<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'name', 'description', 'main_image', 'status', 'tag', 'priority', 'count', 'vote', 'price', 'sell_count', 'ready_duration_time', 'discount', 'batch_id'
    ];

    public function batch()
    {
        return $this->belongsTo('App\Models\Batch');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\ItemComment');
    }

    public function images()
    {
        return $this->hasMany('App\Models\ItemImage');
    }
}
