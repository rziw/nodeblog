<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    protected $fillable = [
        'name', 'status'
    ];

    public function user_accesses()
    {
        return $this->hasMany('App\Models\UserAccess');
    }
}
