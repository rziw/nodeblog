<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategorySections extends Model
{
    protected $fillable = [
        'name', 'image', 'status', 'description', 'category_id'
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}
