<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemComment extends Model
{
    protected $fillable = [
        'title', 'content', 'status', 'item_id', 'user_id'
    ];

    public function item()
    {
        return $this->belongsTo('App\Models\Item');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
