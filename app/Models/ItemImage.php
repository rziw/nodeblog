<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemImage extends Model
{
    protected $fillable = [
        'image', 'status', 'item_id'
    ];

    public function item()
    {
        return $this->belongsTo('App\Models\Item');
    }
}
