<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'image', 'status', 'priority'
    ];

    public function  members()
    {
        return $this->hasMany('App\Models\Member');
    }

    public function  sections()
    {
        return $this->hasMany('App\Models\CategorySections');
    }
}
