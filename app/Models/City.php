<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'name', 'image', 'province_id'
    ];

    public function province()
    {
        return $this->belongsTo('App\Models\City');
    }
}
