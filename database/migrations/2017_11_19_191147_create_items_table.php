<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('main_image')->nullable();
            $table->string('status')->nullable();
            $table->string('tag')->nullable();
            $table->integer('priority');
            $table->integer('count')->default(0);
            $table->integer('vote')->default(0);
            $table->integer('price');
            $table->integer('sell_count')->default(0);
            $table->integer('ready_duration_time')->nullable();
            $table->string('discount')->nullable();
            $table->integer('batch_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
